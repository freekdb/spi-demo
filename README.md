# SPI Demo

This is a sample project demoing how SPI in Java can be used to build an application from independently deployable / developabale parts.
Has an example about replacing a legacy feature with a fresh implementation and two plugins.

Notice that there no central list of features/plugins in this app that would force a component developer to also modify (and redeploy) anything outside of a component's project.

Try it out (assuming you have Java 8+ installed):

Clone:
```
git clone https://gitlab.com/riovir/spi-demo.git
cd spi-demo
```

On Windows:
```
echo "No required feature, no plugins"
java -cp demo.jar;bar-plugin.jar Launcher
echo "Legacy feature, no plugins"
java -cp demo.jar;legacy-feature.jar Launcher
echo "New feature, all plugins"
java -cp demo.jar;new-feature.jar;bar-plugin.jar;foo-plugin.jar Launcher
```

On Linux:
```
echo "No required feature, no plugins"
java -cp demo.jar:bar-plugin.jar Launcher
echo "Legacy feature, no plugins"
java -cp demo.jar:legacy-feature.jar Launcher
echo "New feature, all plugins"
java -cp demo.jar:new-feature.jar:bar-plugin.jar:foo-plugin.jar Launcher
```