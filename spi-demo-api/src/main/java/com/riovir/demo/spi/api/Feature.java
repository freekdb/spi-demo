package com.riovir.demo.spi.api;

/**
 * Many features are required by the system to work regardless what implements them.
 */
public interface Feature {
	
	String greetUsers();
	
}
