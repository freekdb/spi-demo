package com.riovir.demo.spi.api;

import java.util.function.Consumer;

/**
 * Plugins are optional, the system is happy to work without them.
 *
 * Typically they add themselves to a bucket of plugins.
 * If the system uses Guice this is what Multibinders are for.
 */
public interface Plugin {
	
	/**
	 * Adds value to the system
	 * @param bucket of plugin goodies
	 */
	void putBusinessValueIn(Consumer<String> bucket);
	
}
