package com.riovir.demo.spi.bar.plugin;

import java.util.function.Consumer;
import java.util.stream.Stream;

import com.riovir.demo.spi.api.Plugin;

public class BarPlugin implements Plugin {
	@Override
	public void putBusinessValueIn(Consumer<String> bucket) {
		Stream.of(
				"- drinks",
				"- nachos",
				"- Flammkuchen"
		).forEach(bucket);
	}
}
